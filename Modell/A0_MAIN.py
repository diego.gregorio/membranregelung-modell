# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>
#
#
# Author: Diego Gregorio (2020)
#
# MEMBRANREGELUNG MODELL
# MSc Projekt 2020 Umweltingenieurwissenschaften ETH Zürich
#
# A0_MAIN - Simulationsprogramm


# ----------------------------------------------------------------------------------------------------------------------


# region INDEX
# Überblick der Programmstruktur


# 1. PACKAGES
#    Nötige Funktionen und Packages laden
# 2. EINGABE
#    TODO: File paths eingeben
# 3. EINLESEN
#    Kennzahlen und Zufluss-Ganglinie einlesen
# 4. REGELUNG
#    Fixe Parameter für Regelung
# 5. SIMULATION
#    Herzstück
# 6. SCHNELLE OUTPUTS
#    Ausgabe von Kontroll-Kennzahlen
# 7. OUTPUTS
#    Speicherung von Datensätze zur weiteren Analyse
# REFERENZEN


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 1. PACKAGES
# Nötige Python Funktionen und Packages importieren


# Funktionen
from A1_waermepumpe import waermepumpe
from A2_energie import energie


# Packages - Warnung ausgeben falls nicht installiert (Ref [5] & [6])

# date (um jetzige Uhrzeit und Datum zu evaluieren)
try:
    from datetime import datetime
except ImportError as e:
    print('\033[93m' + "datetime nicht installiert! 'pip install datetime' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# matplotlib.pyplot (um Plots zu erstellen)
try:
    import matplotlib.pyplot as plt
except ImportError as e:
    print('\033[93m' + "matplotlib nicht installiert! 'pip install matplotlib' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# numpy (um Berechnungen mit arrays durchzuführen, z.B. cumsum())
try:
    import numpy
except ImportError as e:
    print('\033[93m' + "numpy nicht installiert! 'pip install numpy' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# os (um Order zum speichern zu erstellen)
try:
    import os
except ImportError as e:
    print('\033[93m' + "os nicht installiert! 'pip install os' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# pandas (um Zufluss von Excel File einlesen und mit Dataframes zu arbeiten)
try:
    import pandas as pd
except ImportError as e:
    print('\033[93m' + "pandas nicht installiert! 'pip install pandas' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# pickle (um Daten zu speichern)
try:
    import pickle
except ImportError as e:
    print('\033[93m' + "pickle nicht installiert! 'pip pickle pandas' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# time (um Simulationszeit zu berechnen)
try:
    import time
except ImportError as e:
    print('\033[93m' + "time nicht installiert! 'pip install time' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# xlrd (um Kennzahlen von Excel File einlesen)
try:
    import xlrd
except ImportError as e:
    print('\033[93m' + "xlrd nicht installiert! 'pip install xlrd' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 2. EINGABE
# TODO: Definiere File Paths zu den verschiedenen Inputs


# File path und Excel-Blatt zu Kennzahlen
path_kennzahlen = "B1_input_kennzahlen.xls"
sheet_kennzahlen = "INPUTS"

# File path, Excel-Blatt und Spaltenname zu Zufluss Ganglinie
path_zufluss = "B2_input_zufluss.csv"
column_zufluss = "Zulauf m3/s"


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 3. EINLESEN
# Gemäss File Path Input Files einlesen


# Kennzahlen
workbook_Kennzahlen = xlrd.open_workbook(path_kennzahlen)
worksheet_Kennzahlen = workbook_Kennzahlen.sheet_by_name(sheet_kennzahlen)

# region Einzelne Zellen einlesen

# Zeit
timestep = worksheet_Kennzahlen.cell(4, 2).value
t_start = worksheet_Kennzahlen.cell(5, 2).value
t_end = worksheet_Kennzahlen.cell(6, 2).value

# Geometrische Details
puffer_h_entlastung = worksheet_Kennzahlen.cell(4, 7).value
puffer_h_max = worksheet_Kennzahlen.cell(5, 7).value
puffer_h_min = worksheet_Kennzahlen.cell(6, 7).value
puffer_h_minmin = worksheet_Kennzahlen.cell(7, 7).value
puffer_A = worksheet_Kennzahlen.cell(8, 7).value
speicher_V = worksheet_Kennzahlen.cell(11, 7).value

# Technische Details
Que = worksheet_Kennzahlen.cell(4, 12).value
waermepumpe_COP = worksheet_Kennzahlen.cell(7, 12).value
waermepumpe_kompressor = worksheet_Kennzahlen.cell(8, 12).value
waermepumpe_Tmin = worksheet_Kennzahlen.cell(9, 12).value
waermepumpe_Tin = worksheet_Kennzahlen.cell(10, 12).value
waermepumpe_c_wasser = worksheet_Kennzahlen.cell(11, 12).value
waermepumpe_rho_wasser = worksheet_Kennzahlen.cell(12, 12).value
strassen = worksheet_Kennzahlen.cell(4, 16).value
membran_stehzeit = worksheet_Kennzahlen.cell(5, 16).value
membran_MixZeit = worksheet_Kennzahlen.cell(6, 16).value
geblaese_LeapHi = worksheet_Kennzahlen.cell(9, 16).value
geblaese_LeapLo = worksheet_Kennzahlen.cell(10, 16).value
geblaese_MixLeap = worksheet_Kennzahlen.cell(11, 16).value
geblaese_p = worksheet_Kennzahlen.cell(12, 16).value
geblaese_eta_LeapHi = worksheet_Kennzahlen.cell(13, 16).value
geblaese_eta_LeapLo = worksheet_Kennzahlen.cell(14, 16).value
geblaese_eta_MixLeap = worksheet_Kennzahlen.cell(15, 16).value
pumpe_Q_LeapHi = worksheet_Kennzahlen.cell(18, 16).value
pumpe_Q_LeapLo = worksheet_Kennzahlen.cell(19, 16).value
pumpe_zyklus = worksheet_Kennzahlen.cell(20, 16).value
pumpe_p_LeapHi = worksheet_Kennzahlen.cell(21, 16).value
pumpe_p_LeapLo = worksheet_Kennzahlen.cell(22, 16).value
pumpe_eta_LeapHi = worksheet_Kennzahlen.cell(23, 16).value
pumpe_eta_LeapLo = worksheet_Kennzahlen.cell(24, 16).value

# endregion

# Zufluss Ganglinie
zufluss = pd.read_csv(path_zufluss)

# Alarm ausgeben falls Zufluss-Ganglinie kürzer als Endsimulationszeit
if zufluss.size < t_end/timestep:
    print('\033[93m' + "ACHTUNG: ZUFLUSS-GANGLINIE ERREICHT ENDZEIT DER SIMULATION NICHT (zufluss.size < t_end/timestep)!\n\
        ABBRUCH..." + '\033[0m')
    exit()


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 4. REGELUNG
# Fixe Parameter für Regelung definieren


# Wasserstand Spielraum
delta_h = puffer_h_max - puffer_h_min
delta_h_punkte = delta_h / (strassen * 2)  # Pro Strasse 2 Betriebspunkte

# Betriebspunkte (Anzahl = Anzahl Strassen * 2 + 1)
betriebspunkte = list(range(int(strassen) * 2 + 1))  # 0, 1, 2, ... , strassen*2
betriebshoehen = [(bp * delta_h_punkte + puffer_h_min) for bp in betriebspunkte]

# Durchflüsse und Luftmengen bei den Betriebspunkten initialisieren
betriebsQ = list([0])
betriebsL = list([0])

# Hälfte der Betriebspunkte
bp_half = int(len(betriebspunkte) / 2)  # = strassen

# Leap-Lo in unterer Hälfte der Betriebspunkte
for ii in range(bp_half):
    jj = ii + 1  # Python beginnt loop mit 0
    betriebsQ.append(jj * pumpe_Q_LeapLo)
    betriebsL.append(jj * geblaese_LeapLo)

# Leap-Hi werden progressiv dazugeschaltet in der oberen Hälfte der Betriebspunkte
for ii in range(bp_half):
    jj = ii + 1
    betriebsQ.append((bp_half - jj) * pumpe_Q_LeapLo + jj * pumpe_Q_LeapHi)
    betriebsL.append((bp_half - jj) * geblaese_LeapLo + jj * geblaese_LeapHi)

# Alle Listen in ein Dataframe zusammenfügen (Ref [1])
betrieb = pd.DataFrame(list(zip(betriebspunkte, betriebshoehen, betriebsQ, betriebsL)),
                       columns=['Punkt', 'Hoehen', 'Q', 'L'])

# Kontroll-Tabelle für Zwischenbelüftung-Vermeidung
kontrolle_strasse = list(range(1, int(strassen) + 1))
kontrolle_betrieb = [False] * int(strassen)
kontrolle_zyklus = [0] * int(strassen)
kontrolle_stehzeit = [0] * int(strassen)
kontrolle = pd.DataFrame(list(zip(kontrolle_strasse, kontrolle_betrieb, kontrolle_zyklus, kontrolle_stehzeit)),
                         columns=['Strasse', 'Betrieb', 'Zykluszeit', 'Stehzeit'])


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 5. SIMULATION
# Simulation (Herzstück)


# Zeit aufnehmen
clock_start = time.time()

# Listen definieren für spätere outputs Analyse
# Hydraulische Details
out_h = []  # Wasserspiegel
out_Qin = []  # Zufluss
out_Qe = []  # Entlastung
out_Qab = []  # Abfluss
out_Que = []  # Überschussschlamm
out_L_betrieb = []  # Luftmengen bei Betrieb in [Nm3] nicht [Nm3/s]
out_L_mixing = []  # Luftmengen bei Zwischenbelüftung in [Nm3] nicht [Nm3/s]
out_bp = []  # Betriebspunkte
out_leap_lo = []  # Anzahl Strassen in LEAP-Lo
out_leap_hi = []  # Anzahl Strassen in LEAP-Hi
out_e_geblaese = []  # Energie Betriebsbelüftung
out_e_mixing = []  # Energie Zwischenbelüftung
out_e_pumpen = []  # Energie Pumpen
out_e_waermepumpe = []  # Energie Wärmepumpe

# Variablen initialisieren (falls nötig) oder definieren
# Wasserstand
h_t = puffer_h_min
h_0 = h_t  # Um Volumenveränderung zu berechnen (siehe SCHNELLE OUTPUTS)
# Volumina
V_t = puffer_A * (h_t - puffer_h_minmin)  # Puffervolumen betrifft nur h > h_minmin !
V_entlastung = puffer_A * (puffer_h_entlastung - puffer_h_minmin)
# Strassen in Betrieb bei Zeit t-1
s_t = 0
# Wassertemperatur im Speicher
speicher_T = waermepumpe_Tin

# for-loop
for t in range(int(t_start), int(t_end), int(timestep)):

    ################
    # region Einzelne Durchflüsse berechnen

    # Zufluss
    ii = int(t / timestep)
    Qin_t = zufluss.iloc[ii][column_zufluss]  # Name der Spalte nötig um Wert zu extrahieren

    # Überschusschlamm
    Que_t = Que

    # Notentlastung
    if V_t > V_entlastung:
        Qe_t = (V_t - V_entlastung) / timestep  # [m3/s]
    else:
        Qe_t = 0

    # endregion

    ################
    # region REGELUNG

    # Betrieb gemäss Wasserstand definieren (Ref [2])
    betrieb_t = betrieb.loc[(numpy.searchsorted(betrieb.Hoehen.values, h_t) - 1).clip(0), :]
    Qab_t = betrieb_t.Q
    L_betrieb_t = betrieb_t.L  # Luftdurchfluss [Nm3/s]
    L_betrieb_t_tot = betrieb_t.L * timestep  # Totale Luftmenge [Nm3]

    # Zwischenbelüftungen vermeiden wenn möglich (Ref [3] & [4])
    if betrieb_t.Punkt <= bp_half:  # Untere Hälfte der Betriebspunkte

        # Neue Strasse wird eingeschaltet -> die mit der höchsten Stehzeit auswählen
        if betrieb_t.Punkt > s_t:
            for bb in range(int(betrieb_t.Punkt - s_t)):  # Wie viele Strassen eingeschaltet werden müssen
                mask = ((kontrolle.Betrieb == False) & (kontrolle.Stehzeit == max(kontrolle.Stehzeit)))\
                           .cumsum().cumsum() == 1  # Erster Match (Ref [4])
                kontrolle.loc[mask, 'Betrieb'] = True
                kontrolle.loc[mask, 'Stehzeit'] = 0  # Damit max() nicht beeinflusst wird
                s_t = s_t + sum(mask)  # Strassen in Betrieb (für nächsten Zeitschritt)

        # Strasse wird abgeschaltet -> die mit der höchsten Zykluszeit (unter Einhaltung der minimalen Dauer) auswählen
        elif betrieb_t.Punkt < s_t:
            for bb in range(int(s_t - betrieb_t.Punkt)):  # Wie viele Strassen ausgeschaltet werden müssen
                mask = ((kontrolle.Betrieb == True) & (kontrolle.Zykluszeit == max(kontrolle.Zykluszeit)) & (kontrolle.Zykluszeit >= pumpe_zyklus))\
                           .cumsum().cumsum() == 1  # Erster Match (Ref [4])
                kontrolle.loc[mask, 'Betrieb'] = False
                kontrolle.loc[mask, 'Zykluszeit'] = 0  # Damit max() nicht beeinflusst wird
                s_t = s_t - sum(mask)  # Strassen in Betrieb (für nächsten Zeitschritt)

        # Bei Zyklus Einhaltung, Betrieb auf neue Strasse verlagern
        mask1 = ((kontrolle.Betrieb == True) & (kontrolle.Zykluszeit >= pumpe_zyklus))  # Strassen die abgeschaltet werden können
        mask2 = ((kontrolle.Betrieb == False) & (kontrolle.Stehzeit > 0))  # Strassen die angeschaltet werden können
        for bb in range(min(sum(mask1), sum(mask2))):  # Wie viele Switch möglich
            mask1bb = ((kontrolle.Betrieb == True) & (kontrolle.Zykluszeit == max(kontrolle.Zykluszeit)) & (kontrolle.Zykluszeit >= pumpe_zyklus))\
                          .cumsum().cumsum() == 1  # Erster Match (Ref [4])
            mask2bb = ((kontrolle.Betrieb == False) & (kontrolle.Stehzeit == max(kontrolle.Stehzeit)) & (kontrolle.Stehzeit > 0))\
                          .cumsum().cumsum() == 1  # Erster Match (Ref [4])
            kontrolle.loc[mask1bb, 'Betrieb'] = False
            kontrolle.loc[mask1bb, 'Zykluszeit'] = 0  # Damit max() nicht beeinflusst wird
            kontrolle.loc[mask2bb, 'Betrieb'] = True
            kontrolle.loc[mask2bb, 'Stehzeit'] = 0  # Damit max() nicht beeinflusst wird

        # Zwischenbelüftung wo nötig
        mask = ((kontrolle.Betrieb == False) & (kontrolle.Stehzeit >= membran_stehzeit))
        kontrolle.loc[mask, 'Stehzeit'] = - membran_MixZeit  # Zwischenebelüftungszeit berücksichtigen
        L_mix_t = sum(mask) * geblaese_MixLeap  # Luftdurchfluss [Nm3/s]
        L_mix_t_tot = L_mix_t * membran_MixZeit  # Totale Luftmenge [Nm3]

    # Obere Hälfte der Betriebspunkte
    else:
        # Alle Strassen in Betrieb
        s_t = strassen
        kontrolle.Betrieb = True

        # Keine Zwischenbelüftung
        L_mix_t = 0
        L_mix_t_tot = 0

    # Zyklus-Zeiten und Stehzeiten aktualisieren gemäss Betrieb
    mask = (kontrolle.Betrieb == True)
    kontrolle.loc[mask, 'Zykluszeit'] += timestep
    kontrolle.loc[mask, 'Stehzeit'] = 0
    kontrolle.loc[~mask, 'Zykluszeit'] = 0
    kontrolle.loc[~mask, 'Stehzeit'] += timestep

    # endregion

    ################
    # region ÜBERWACHUNG DER REGELUNG

    # Zeitschritt, Betriebspunkt und Kontroll-Tabelle ausgeben TODO - Kommentieren falls nicht erwünscht
    print("Zeitschritt=", t)
    print("Betriebspunkt=", betrieb_t.Punkt)
    print(kontrolle)

    # Alarm ausgeben falls Strassenbetrieb falsch kontrolliert wird
    if s_t > bp_half:
        print('\033[93m' + "ACHTUNG: BETRIEBSPUNKTE-KONTROLLE FALSCH: s_t KANN NICHT > strassen SEIN!\n\
            ABBRUCH...\n\
            Zeitschritt = " + str(t) + "s" + '\033[0m')
        break
    if (s_t <= strassen) & (sum(kontrolle.Betrieb) != s_t):
        print('\033[93m' + "ACHTUNG: STRASSEN IN BETRIEB SIND FALSCH ANGEGEBEN IN KONTROLL-TABELLE!\n\
            Zwischeneblüftungsvermeidung wohl Fehlgeschlagen,\n\
            Es sind zu wenig Strassen in Betrieb gemäss Kontroll-Tabelle, oder s_t falsch aktualisiert\n\
            ABBRUCH...\n\
            Zeitschritt = " + str(t) + "s" + '\033[0m')
        break
    elif (betrieb_t.Punkt > bp_half) & (sum(kontrolle.Betrieb) < strassen):
        print('\033[93m' + "ACHTUNG: STRASSEN IN BETRIEB SIND FALSCH ANGEGEBEN IN KONTROLL-TABELLE!\n\
            In oberen Hälfte der Betriebspunkte müssen alle Strassen in Betrieb sein\n\
            ABBRUCH...\n\
            Zeitschritt = " + str(t) + "s" + '\033[0m')
        break

    # endregion

    ################
    # region Wasserspiegel berechnen

    # Netto Durchfluss Bilanz
    Qnetto_t = Qin_t - Qab_t - Que_t - Qe_t

    # Volumenstand
    V_t = V_t + (Qnetto_t * timestep)

    # Wasserstand
    h_t = V_t / puffer_A + puffer_h_minmin

    # endregion

    ################
    # region Energie berechnen

    # Gebläse Membran bei Betrieb
    e_geblaese = energie(betrieb_t.Punkt, strassen, geblaese_LeapHi, geblaese_LeapLo, geblaese_p, geblaese_p,
                         geblaese_eta_LeapHi, geblaese_eta_LeapLo, timestep)

    # Gebläse Membran bei Zwischenbelüftung
    e_mixing = energie(1, strassen, L_mix_t, L_mix_t, geblaese_p, geblaese_p,
                         geblaese_eta_MixLeap, geblaese_eta_MixLeap, membran_MixZeit)

    # Pumpe Membran
    e_pumpen = energie(betrieb_t.Punkt, strassen, pumpe_Q_LeapHi, pumpe_Q_LeapLo, pumpe_p_LeapHi, pumpe_p_LeapLo,
                       pumpe_eta_LeapHi, pumpe_eta_LeapLo, timestep)

    # Wärmepumpe
    waermegewinn, speicher_T = waermepumpe(speicher_T, waermepumpe_Tmin, waermepumpe_COP, waermepumpe_kompressor, Qab_t,
                                           waermepumpe_Tin, speicher_V, waermepumpe_c_wasser, waermepumpe_rho_wasser,
                                           timestep)
    e_waermepumpe = waermegewinn * timestep/60/60  # von [kW] zu [kWh] unter Berücksichtigung der Betriebsdauer in [h]

    # endregion

    ################
    # region Relevante Resultate speichern

    out_h.append(h_t)
    out_Qin.append(Qin_t)
    out_Qe.append(Qe_t)
    out_Qab.append(Qab_t)
    out_Que.append(Que_t)
    out_L_betrieb.append(L_betrieb_t_tot)
    out_L_mixing.append(L_mix_t_tot)
    out_bp.append(betrieb_t.Punkt)
    out_e_geblaese.append(e_geblaese)
    out_e_mixing.append(e_mixing)
    out_e_pumpen.append(e_pumpen)
    out_e_waermepumpe.append(e_waermepumpe)

    # Anzahl LEAP-Hi/Lo
    if betrieb_t.Punkt <= bp_half:  # Nur Strassen in LEAP-Lo
        out_leap_lo.append(betrieb_t.Punkt)
        out_leap_hi.append(0)
    else:  # Mindestens eine Strasse LEAP-Hi, die restlichen LEAP-Lo
        out_leap_lo.append(2 * bp_half - betrieb_t.Punkt)
        out_leap_hi.append(betrieb_t.Punkt - bp_half)

    # endregion


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 6. SCHNELLE OUTPUTS
# Generierung von wichtigen Kontroll-Kennzahlen


# Ausgaben auf Konsole (Ref[5])

# Simulation is fertig
print('\033[95m' + "SIMULATION ABGESCHLOSSEN" + '\033[95m')  # (Ref [5])

# Zeitaufwand ausgeben
clock_end = time.time()
elapsed_minutes = (clock_end - clock_start) / 60
print('\033[94m' + "Simulation benötigte " + str(int(elapsed_minutes)) + " min" + '\033[94m')

# Dauer [min] von h_t < h_minmin ausgeben
h_minmin_dauer = len([ii for ii in out_h if ii < puffer_h_minmin]) * timestep/60
print('\033[96m' + "Zeit h < h_minmin = " + str(int(h_minmin_dauer)) + " min" + '\033[96m')

# Dauer [min] und Volumen [m3] von Entlastungen ausgeben
entlastungen_dauer = len([ii for ii in out_h if ii > puffer_h_entlastung]) * timestep/60
entlastungen_volumen = numpy.sum(out_Qe) * timestep
print('\033[96m' + "Entlastungsdauer = " + str(int(entlastungen_dauer)) + " min" + '\033[96m')
print('\033[96m' + "Entlastungsvolumen = " + str(int(entlastungen_volumen)) + " m3" + '\033[96m')

# Volumenbilanz [m3]
zufluss_tot = numpy.sum(out_Qin) * timestep
abfluss_tot = numpy.sum(out_Qab) * timestep
ueberschuss_tot = numpy.sum(out_Que) * timestep
volumen_veraenderung = numpy.sum((out_h[-1] - h_0) * puffer_A)  # Volumen Veränderung (V_end - V_start)
print('\033[92m' + "Zufluss = " + str(int(zufluss_tot)) + " m3" + '\033[92m')
print('\033[92m' + "Abfluss = " + str(int(abfluss_tot)) + " m3" + '\033[92m')
print('\033[92m' + "Überschussschlamm = " + str(int(ueberschuss_tot)) + " m3" + '\033[92m')
print('\033[92m' + "Volumen Veränderung = " + str(int(volumen_veraenderung)) + " m3" + '\033[92m')
print('\033[92m' + "Volumen Bilanz = " + str(int(zufluss_tot - abfluss_tot - ueberschuss_tot - volumen_veraenderung)) + " m3 (= Entlastung)" + '\033[92m')


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 7. OUTPUTS
# Speicherung von Datensätze zur weiteren Analyse


# Unterordner mit Endzeit der Simulation in Output Ordner erstellen
date_now = datetime.now().strftime('%Y%m%d %H%M%S')  # (Ref [7])
save_path = "XX_outputs/A0_main/" + str(date_now)
os.makedirs(save_path)

# Alle relevanten outputs speichern als pickle (Ref [8]) in Ordner mit Endzeit der Simulation
pickle.dump(out_h, open(save_path + "/out_h.p", "wb"))
pickle.dump(out_Qin, open(save_path + "/out_Qin.p", "wb"))
pickle.dump(out_Qe, open(save_path + "/out_Qe.p", "wb"))
pickle.dump(out_Qab, open(save_path + "/out_Qab.p", "wb"))
pickle.dump(out_Que, open(save_path + "/out_Que.p", "wb"))
pickle.dump(out_L_betrieb, open(save_path + "/out_L_betrieb.p", "wb"))
pickle.dump(out_L_mixing, open(save_path + "/out_L_mixing.p", "wb"))
pickle.dump(out_bp, open(save_path + "/out_bp.p", "wb"))
pickle.dump(out_e_geblaese, open(save_path + "/out_e_geblaese.p", "wb"))
pickle.dump(out_e_mixing, open(save_path + "/out_e_mixing.p", "wb"))
pickle.dump(out_e_pumpen, open(save_path + "/out_e_pumpen.p", "wb"))
pickle.dump(out_e_waermepumpe, open(save_path + "/out_e_waermepumpe.p", "wb"))
pickle.dump(out_leap_lo, open(save_path + "/out_leap_lo.p", "wb"))
pickle.dump(out_leap_hi, open(save_path + "/out_leap_hi.p", "wb"))

# Speichere Input-Kennzahlen um Simulation zu erkennen (Ref [9])
text_file = open(save_path + "/inputs.txt", "w")
text_file.write('timestep [s] = ' + str(timestep) + '\nt start [s] = ' + str(t_start) + '\nt end [s] = ' + str(t_end)
                + '\n\nh_entlastung [m] = ' + str(puffer_h_entlastung) + '\nh_max [m] = ' + str(puffer_h_max)
                + '\nh_min [m] = ' + str(puffer_h_min) + '\nh_minmin [m] = ' + str(puffer_h_minmin)
                + '\nA [m2] = ' + str(puffer_A)
                + '\n\nVspeicher [m3] = ' + str(speicher_V) + '\nQue [m3/s] = ' + str(Que)
                + '\n\nWärmepumpe COP [-] = ' + str(waermepumpe_COP) + '\nWärmepumpe Verbrauch [kW] = ' + str(waermepumpe_kompressor)
                + '\nWärmepumpe Tmin [°C] = ' + str(waermepumpe_Tmin) + '\nWärmepumpe Tin [°C] = ' + str(waermepumpe_Tin)
                + '\nWärmepumpe c_H2O [kJ/kg/K] = ' + str(waermepumpe_c_wasser) + '\nWärmepumpe rho_H2O [kg/m3] = ' + str(waermepumpe_rho_wasser)
                + '\n\nMembran Strassen [-] = ' + str(strassen) + '\nMembran Stehzeit [s] = ' + str(membran_stehzeit)
                + '\nMembran Mixing Zeit [s] = ' + str(membran_MixZeit) + '\n\nLEAP-Hi [Nm3/s] = ' + str(geblaese_LeapHi)
                + '\nLEAP-Lo [Nm3/s] = ' + str(geblaese_LeapLo) + '\nMixing LEAP [Nm3/s] = ' + str(geblaese_MixLeap)
                + '\nDruckhöhe Gebläse [m] = ' + str(geblaese_p) + '\nWirkungsgrad Gebläse Leap-Hi [-] = ' + str(geblaese_eta_LeapHi)
                + '\nWirkungsgrad Gebläse Leap-Lo [-] = ' + str(geblaese_eta_LeapLo) + '\nWirkungsgrad Mixing Leap [-] = ' + str(geblaese_eta_MixLeap)
                + '\n\nQ_Leap-Hi [m3/s] = ' + str(pumpe_Q_LeapHi) + '\nQ_Leap-Lo [m3/s] = ' + str(pumpe_Q_LeapLo)
                + '\nMinimale Zykluszeit [s] = ' + str(pumpe_zyklus) + '\nDruckhöhe Pumpe Leap-Hi [m] = ' + str(pumpe_p_LeapHi)
                + '\nDruckhöhe Pumpe Leap-Lo [m] = ' + str(pumpe_p_LeapLo) + '\nWirkungsgrad Pumpe Leap-Hi [-] = ' + str(pumpe_eta_LeapHi)
                + '\nWirkungsgrad Pumpe Leap-Lo [-] = ' + str(pumpe_eta_LeapLo)
                )
text_file.close()


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region REFERENZEN
# Quellen (im Code angegeben als "Ref [n]")


# [1]
# "Create a Pandas DataFrame from Lists", GeeksForGeeks, 2018
# https://www.geeksforgeeks.org/create-a-pandas-dataframe-from-lists/
# Accessed 26.10.2020

# [2]
# "Python Pandas Dataframe: Take Greatest Value Smaller Than", Stackoverflow, 2017
# https://stackoverflow.com/questions/47501177/python-pandas-dataframe-take-greatest-value-smaller-than
# Accessed 26.10.2020

# [3]
# "Conditional Replace Pandas", Stackoverflow, 2014
# https://stackoverflow.com/questions/21608228/conditional-replace-pandas
# Accessed 26.10.2020

# [4]
# "How to obtain only the first True value from each row of a numpy array?", Stackoverflow, 2019
# https://stackoverflow.com/questions/54155844/how-to-obtain-only-the-first-true-value-from-each-row-of-a-numpy-array
# Accessed 26.10.2020

# [5]
# "How to print colored text in Python?", Stackoverflow, 2008
# https://stackoverflow.com/questions/287871/how-to-print-colored-text-in-python
# Accessed 28.10.2020

# [6]
# "Check if Python Package is installed", Stackoverflow, 2009
# https://stackoverflow.com/questions/1051254/check-if-python-package-is-installed
# Accessed 28.10.2020

# [7]
# "How to get current date and time in Python?", Programiz, 2019
# https://www.programiz.com/python-programming/datetime/current-datetime
# Accessed 30.10.2020

# [8]
# "Using pickle", wiki python, 2019
# https://wiki.python.org/moin/UsingPickle
# Accessed 30.10.2020

# [9]
# "How to Write String to Text File in Python?", Python Examples, 2020
# https://pythonexamples.org/python-write-string-to-text-file/
# Accessed 02.11.2020


# endregion
