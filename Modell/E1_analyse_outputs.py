# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>
#
#
# Author: Diego Gregorio (2020)
#
# MEMBRANREGELUNG MODELL
# MSc Projekt 2020 Umweltingenieurwissenschaften ETH Zürich
#
# E1_analyse_outputs - Schlussanalyse der generierten Outputs aus der Simulation


# ----------------------------------------------------------------------------------------------------------------------


# region INDEX
# Überblick der Programmstruktur


# 1. PACKAGES
#    Nötige Funktionen und Packages laden
# 2. EINGABE
#    TODO: User Inputs
# 3. IMPORT
#    Outputs aus Simulation einlesen
# 4. ANALYSE - RAHMBEDINGUNGEN
#    Volumenbilanz und Nullpunkt Einhaltung
# 5. ANALYSE - ALLGEMEIN
#    Wasserspiegel und Energiebilanz
# 6. ANALYSE - REGELUNG
#    Betriebspunkte und Zwischenbelüftungen
# 7. ANALYSE - MEMBRANPUMPEN
#    Abfluss und Energieaufwand
# 8. ANALYSE - MEMBRANGEBLÄSE
#    Luftmenge und Energieaufwand
# 9. ANALYSE - WÄRMEPUMPE
#    Stehzeit und Energiebilanz
# 10. KENNZAHLEN SPEICHERN
#    Evaluierte Kennzahlen in .txt File speichern
# 11. ANZEIGE
#    Zeige plots an (falls)
# REFERENZEN


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 1. PACKAGES
# Nötige Funktionen und Packages laden


# Funktion plot_haeufigkeit
from E2_plot_haeufigkeit import plot_haeufigkeit


# matplotlib.pyplot (um Plots zu erstellen) und matplotlib.colors (um Colormap zu erstellen)
try:
    import matplotlib.pyplot as plt
except ImportError as e:
    print('\033[93m' + "matplotlib nicht installiert! 'pip install matplotlib' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.
from matplotlib.colors import LinearSegmentedColormap

# numpy (um Berechnungen mit arrays durchzuführen)
try:
    import numpy
except ImportError as e:
    print('\033[93m' + "numpy nicht installiert! 'pip install numpy' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# os (um Order zum speichern zu erstellen)
try:
    import os
except ImportError as e:
    print('\033[93m' + "os nicht installiert! 'pip install os' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# pandas (um mit Serien/Dataframes zu arbeiten)
try:
    import pandas as pd
except ImportError as e:
    print('\033[93m' + "pandas nicht installiert! 'pip install pandas' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# pickle (um gespeicherte Daten zu importieren)
try:
    import pickle
except ImportError as e:
    print('\033[93m' + "pickle nicht installiert! 'pip install pickle' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 2. EINGABE
# TODO: User Inputs


# Zu analysierender Datensatz Path und Path zum speichern der Resultate
sim_path = "XX_outputs/A0_main/V0_Referenz"
save_path = "XX_outputs/E1_outputs/V0_Referenz"

# Simulationszeitschritt in [s] und [a]
timestep_s = 5 * 60  # (5min)
timestep_a = 5 / 60 / 24 / 365

# Initialisierungszeit in [s] (wird abgeschnitten)
init = 1 * 30 * 24 * 60 * 60  # 1 Monat

# Spezifische Inputs (können im input.txt der Simulation gefunden werden)
h_minmin = 3.8000000000000114  # [m] Nullpunkt (h_minmin)
puffer_A = 896.44  # [m2] Pufferfläche
strassen = 4  # [-] Anzahl Membranstrassen
mixing_t = 2 * 60  # [s] Zwischenbelüftungsdauer

# Gewollte Plot Dimensionen [Zoll], Schriftart (Ref [3])
size_fig = [4.5, 3]  # Breite, Höhe
plt.rcParams["font.family"] = "arial"
plt.rcParams["font.size"] = 11

# Ordner erstellen um Kennzahlen und Plots zu speichern
if not os.path.exists(save_path):
    os.makedirs(save_path)
    os.makedirs(save_path + "/Plots")


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 3. IMPORT
# Outputs aus Simulation einlesen


# Initialisierung in Datensatz-Zeitschritte
init = int(init / timestep_s)

# Datensätze einlesen, in Pandas Serien umwandeln (Ref [1]) und Initialisierungszeit wegschneiden
out_bp = pd.Series(pickle.load(open(sim_path + "/out_bp.p", "rb"))).iloc[init:]
out_e_geblaese = pd.Series(pickle.load(open(sim_path + "/out_e_geblaese.p", "rb"))).iloc[init:]
out_e_mixing = pd.Series(pickle.load(open(sim_path + "/out_e_mixing.p", "rb"))).iloc[init:]
out_e_pumpen = pd.Series(pickle.load(open(sim_path + "/out_e_pumpen.p", "rb"))).iloc[init:]
out_e_waermepumpe = pd.Series(pickle.load(open(sim_path + "/out_e_waermepumpe.p", "rb"))).iloc[init:]
out_h = pd.Series(pickle.load(open(sim_path + "/out_h.p", "rb"))).iloc[init:]
out_L_betrieb = pd.Series(pickle.load(open(sim_path + "/out_L_betrieb.p", "rb"))).iloc[init:]
out_L_mixing = pd.Series(pickle.load(open(sim_path + "/out_L_mixing.p", "rb"))).iloc[init:]
out_leap_lo = pd.Series(pickle.load(open(sim_path + "/out_leap_lo.p", "rb"))).iloc[init:]
out_leap_hi = pd.Series(pickle.load(open(sim_path + "/out_leap_hi.p", "rb"))).iloc[init:]
out_Qab = pd.Series(pickle.load(open(sim_path + "/out_Qab.p", "rb"))).iloc[init:]
out_Qe = pd.Series(pickle.load(open(sim_path + "/out_Qe.p", "rb"))).iloc[init:]
out_Qin = pd.Series(pickle.load(open(sim_path + "/out_Qin.p", "rb"))).iloc[init:]
out_Que = pd.Series(pickle.load(open(sim_path + "/out_Que.p", "rb"))).iloc[init:]

# Zeitvektor erstellen
timevec = numpy.arange(0, out_bp.size*timestep_a, timestep_a)
kennzahl_t = timevec.size * timestep_a  # Totale simulierte Zeit, damit Kennzahlen auf [1/a]


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 4. ANALYSE - RAHMBEDINGUNGEN
# Volumenbilanz und Wasserspiegel unter Nullpunkt


# Kennzahlen (Zufluss ist ein Zeitschritt voraus)
kennzahl_Vin = numpy.sum(out_Qin.iloc[:-1]) * timestep_s / kennzahl_t  # Zufluss
kennzahl_Vab = numpy.sum(out_Qab.iloc[1:]) * timestep_s / kennzahl_t  # Abfluss
kennzahl_Vue = numpy.sum(out_Que.iloc[1:]) * timestep_s / kennzahl_t  # Überschussschlamm
kennzahl_V = (out_h.iloc[-1] - out_h.iloc[0]) * puffer_A / kennzahl_t  # Volumenveränderung end vs. start
kennzahl_Ve = numpy.sum(out_Qe.iloc[1:]) * timestep_s / kennzahl_t  # Entlastete Volumine
kennzahl_te = out_Qe[out_Qe.gt(0)].size * timestep_s / kennzahl_t  # Entlastungszeit
kennzahl_tmin = out_h[out_h.lt(h_minmin)].size * timestep_s / kennzahl_t  # Zeit Wasserspiegel unter Nullpunkt

# Plot - Häufigkeitsverteilung der Zuflusses
fig, _, _ = plot_haeufigkeit(out_Qin, 'Zufluss [$\mathregular{m^3}$/s]', figsize=size_fig)
fig.savefig(save_path + "/Plots/Rahmbedingung_Qin.svg")

# Plot - Häufigkeitsverteilung der entlasteten Volumina
fig, _, _ = plot_haeufigkeit(out_Qe, 'Notentlastung [$\mathregular{m^3}$/s]', figsize=size_fig)
fig.savefig(save_path + "/Plots/Rahmbedingung_Qe.svg")


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 5. ANALYSE - ALLGEMEIN
# Wasserstand und Energiebilanz


# Kennzahlen
out_e = out_e_waermepumpe - out_e_pumpen - out_e_geblaese - out_e_mixing
kennzahl_E = numpy.sum(out_e) / kennzahl_t  # Energiebilanz

# Plot - Häufigkeitsverteilung der Energiebilanz
fig, _, _ = plot_haeufigkeit(out_e, "Energiebilanz [kWh]", figsize=size_fig)
fig.savefig(save_path + "/Plots/Allgemein_E.svg")

# Plot - Häufigkeitsverteilung der Wasserspiegel
fig, _, _ = plot_haeufigkeit(out_h, "Wasserspiegel [m]", figsize=size_fig)
fig.savefig(save_path + "/Plots/Allgemein_h.svg")


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 6. ANALYSE - REGELUNG
# Betriebspunkte und Zwischenbelüftungen


# Kennzahlen
kennzahl_mixing = out_L_mixing[out_L_mixing.gt(0)].size / kennzahl_t  # Anzahl Zwischenbelüftungen
kennzahl_betriebsdauer = out_Qab[out_Qab.gt(0)].size * timestep_a / kennzahl_t  # Betriebsdauer

# Plot - Häufigkeitsverteilung der Betriebspunkte
fig, _, _ = plot_haeufigkeit(out_bp, "Betriebspunkt", figsize=size_fig, bins=numpy.insert(numpy.unique(out_bp), 0, numpy.amin(numpy.array(out_bp)) - 1) + 0.5)  # Bin Edges setzen (minimaler Wert -1 einsetzen, damit unterster Edge, dann alle Werte +0.5, damit Bins zentriert)
fig.savefig(save_path + "/Plots/Regelung_bp.svg")


##########################################################
# region Plot - Wasserspiegel vs. Betriebspunkt (Ref [5], [6], [7] & [8])

# Häufigkeitsverteilung des Wasserspiegels
fig, ax1, ax2 = plot_haeufigkeit(out_h, "Wasserspiegel [m]", figsize=size_fig)
ax2.remove()  # Kumulativer Zeitanteil verwerfen

# Neue zweite Y-Axis vorbereiten (Ref [8])
color = 'xkcd:copper'
ax2 = ax1.twinx()
ax2.set_ylabel('Betriebspunkt', color=color)
ax2.tick_params(axis='y', labelcolor=color)

# Colormap mit Anfangsdurchsichtigkeit (Ref [7])
ncolors = 256
color_array = plt.get_cmap('copper_r')(range(ncolors))  # get colormap
color_array[:, -1] = numpy.concatenate([numpy.linspace(0.0, 1.0, 10), numpy.ones(246)])  # change alpha values of first ten colormap values
map_object = LinearSegmentedColormap.from_list(name='copper_r_alpha', colors=color_array)  # create a colormap object
plt.register_cmap(cmap=map_object)  # register this new colormap with matplotlib

# Heatmap statt Scatterplot (Ref [5])
heatmap, xedges, yedges = numpy.histogram2d(out_h, out_bp, bins=30)
extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
ax2.imshow(heatmap.T, cmap='copper_r_alpha', extent=extent, origin='lower', aspect='auto')
ax1.autoscale(False)  # (Ref [6])
ax2.autoscale(False)
fig.savefig(save_path + "/Plots/Regelung_h_bp.svg")
# endregion
##########################################################


# Plot - Wasserspiegel und Betriebspunkt über die Zeit
fig, ax1 = plt.subplots(figsize=size_fig)
color = 'tab:blue'
ax1.set_xlabel('Zeit [a]')
ax1.set_ylabel('Wasserspiegel [m]', color=color)
ax1.tick_params(axis='y', labelcolor=color)
ax1.plot(timevec, out_h, color=color)
color = 'xkcd:copper'
ax2 = ax1.twinx()
ax2.set_ylabel('Betriebspunkt', color=color)
ax2.tick_params(axis='y', labelcolor=color)
ax2.plot(timevec, out_bp, color=color, linestyle='--')
ax2.text(timevec[0], numpy.mean(out_bp), "Zoom im interaktiven Fenster nötig\nund manuell speichern", color='red', bbox=dict(facecolor='white'))  # (Ref [4])
fig.tight_layout()
fig.savefig(save_path + "/Plots/Regelung_h_bp_t.svg")

# Plot - Häufigkeitsverteilung der Wasserspiegel während Zwischenbelüftung
fig, _, _ = plot_haeufigkeit(out_h[out_L_mixing.gt(0)], "Wasserspiegel während Zwischenbelüftung [m]", figsize=size_fig)
fig.savefig(save_path + "/Plots/Regelung_h_mixing.svg")

# Plot - Häufigkeitsverteilung LEAP-Hi/Lo
leap_lo_tot = numpy.sum(out_leap_lo / strassen)  # Totale Zeitschritte in LEAP-Lo pro Strasse
leap_hi_tot = numpy.sum(out_leap_hi / strassen)  # Totale Zeitschritte in LEAP-Hi pro Strasse
leap_aus_tot = out_leap_lo.size - leap_lo_tot - leap_hi_tot  # Auszeit pro Strasse (ergibt sich durch Zeitdifferenz)
leap_tot = leap_lo_tot + leap_hi_tot + leap_aus_tot
leap_mixing = out_L_mixing[out_L_mixing.gt(0)].size * mixing_t / timestep_s / strassen  # Zwischenbelüftungszeitschritte (Teil der Auszeit) pro Strasse

color = 'tab:blue'
fig, ax1 = plt.subplots(figsize=size_fig)
ax1.bar("LEAP-Lo", leap_lo_tot/leap_tot, color=color)
ax1.bar("LEAP-Hi", leap_hi_tot/leap_tot, color=color)
ax1.bar("AUS", leap_aus_tot/leap_tot, color=color)
ax1.bar("AUS", leap_mixing/leap_tot, color='tab:red')
ax1.text("AUS", leap_mixing/leap_tot, "Mixing=\n"+str('%.5f'%(leap_mixing/leap_tot)), horizontalalignment='center', verticalalignment='bottom')
ax1.set_ylabel("Zeitanteil pro Strasse [-]")
fig.tight_layout()
fig.savefig(save_path + "/Plots/Regelung_strassen.svg")


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 7. ANALYSE - MEMBRANPUMPEN
# Abfluss und Energieaufwand


# Kennzahlen
kennzahl_0Qab = out_Qab[out_Qab.eq(0)].size * timestep_a / kennzahl_t  # Zeit ohne Abfluss
kennzahl_E_pumpen = numpy.sum(out_e_pumpen) / kennzahl_t  # Energieaufwand

# Plot - Häufigkeitsverteilung Abflussmenge
fig, _, _ = plot_haeufigkeit(out_Qab, "Abfluss (Pumpen) [$\mathregular{m^3}$/s]", figsize=size_fig)
fig.savefig(save_path + "/Plots/Pumpen_Q.svg")

# Plot - Häufigkeitsverteilung Energieaufwand
fig, _, _ = plot_haeufigkeit(out_e_pumpen, "Energieaufwand Pumpen [kWh]", figsize=size_fig)
fig.savefig(save_path + "/Plots/Pumpen_E.svg")


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 8. ANALYSE - MEMBRANGEBLÄSE
# Luftmenge und Energieaufwand


# Kennzahlen
kennzahl_L_betrieb = numpy.sum(out_L_betrieb) / kennzahl_t  # Luftmenge während Pumpen
kennzahl_L_mixing = numpy.sum(out_L_mixing) / kennzahl_t  # Luftmenge für Zwischenbelüftungen
kennzahl_E_geblaese = numpy.sum(out_e_geblaese + out_e_mixing) / kennzahl_t  # Energieaufwand

# Plot - Häufigkeitsverteilung Luftmenge
fig, _, _ = plot_haeufigkeit((out_L_betrieb/timestep_s + out_L_mixing/mixing_t), "Luftmenge [$\mathregular{Nm^3}$/s]", figsize=size_fig)
fig.savefig(save_path + "/Plots/Geblaese_L.svg")

# Plot - Häufigkeitsverteilung Energieaufwand
fig, _, _ = plot_haeufigkeit((out_e_geblaese + out_e_mixing), "Energieaufwand Gebläse [kWh]", figsize=size_fig)
fig.savefig(save_path + "/Plots/Geblaese_E.svg")


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 9. ANALYSE - WÄRMEPUMPE
# Stehzeit und Energiebilanz


# Kennzahlen
kennzahl_waermepumpe_stehzeit = out_e_waermepumpe[out_e_waermepumpe.eq(0)].size * timestep_a / kennzahl_t  # Stehzeit
kennzahl_E_waermepumpe = numpy.sum(out_e_waermepumpe) / kennzahl_t  # Energiegewinn

# Plot - Häufigkeitsverteilung Energiegewinn
fig, _, _ = plot_haeufigkeit(out_e_waermepumpe, "Energiegewinn Wärmepumpe [kWh]", figsize=size_fig)
fig.savefig(save_path + "/Plots/Waermepumpe_E.svg")


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 10. KENNZAHLEN SPEICHERN
# Evaluierte Kennzahlen in .txt File speichern


# Speichern (Ref [2])
text_file = open(save_path + "/Kennzahlen.txt", "w")
text_file.write('Zeit [a] = ' + str('%.4f'%(kennzahl_t))
                + '\n\n------------------------------------------'
                + '\nRAHMBEDINGUNGEN'
                + '\nZufluss [m3/a] = ' + str(int(kennzahl_Vin))
                + '\nAbfluss [m3/a] = ' + str(int(kennzahl_Vab))
                + '\nÜberschussschlamm [m3/a] = ' + str(int(kennzahl_Vue))
                + '\nVolumen Veränderung [m3/a] = ' + str(int(kennzahl_V))
                + '\nNotentlastungen [m3/a] = ' + str(int(kennzahl_Ve))
                + '\nVolumen Bilanz [m3/a] = ' + str(int(kennzahl_Vin - kennzahl_Vab - kennzahl_Vue - kennzahl_V - kennzahl_Ve))
                + '\n\nNotentlastungsdauer [s/a] = ' + str(int(kennzahl_te))
                + '\nZeit Wasserspiegel < Nullpunkt [s/a] = ' + str(int(kennzahl_tmin))
                + '\n\n------------------------------------------'
                + '\nALLGEMEIN'
                + '\nEnergiebilanz Total [kWh/a] = ' + str(int(kennzahl_E))
                + '\n\n------------------------------------------'
                + '\nREGELUNG'
                + '\nZwischenbelüftungen [1/a] = ' + str('%.2f'%(kennzahl_mixing))
                + '\nBetriebsdauer [a/a] = ' + str('%.4f'%(kennzahl_betriebsdauer))
                + '\n\n------------------------------------------'
                + '\nPUMPEN'
                + '\nEnergieaufwand der Pumpen [kWh/a] = ' + str(int(kennzahl_E_pumpen))
                + '\n\n------------------------------------------'
                + '\nGEBLÄSE'
                + '\nLuftmenge bei Abpumpen [Nm3/a] = ' + str(int(kennzahl_L_betrieb))
                + '\nLuftmenge für Zwischenbelüftung [Nm3/a] = ' + str(int(kennzahl_L_mixing))
                + '\nEnergieaufwand Belüftung [kWh/a] = ' + str(int(kennzahl_E_geblaese))
                + '\n\n------------------------------------------'
                + '\nABFLUSS'
                + '\nAbflussmenge siehe oben'
                + '\nZeit ohne Abfluss [a/a] = ' + str('%.4f'%(kennzahl_0Qab))
                + '\n\n------------------------------------------'
                + '\nWÄRMEPUMPE'
                + '\nStehzeit Wärmepumpe [a/a] = ' + str('%.4f'%(kennzahl_waermepumpe_stehzeit))
                + '\nEnergiegewinn Wärmepumpe [kWh/a] = ' + str(int(kennzahl_E_waermepumpe))
                )
text_file.close()


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 11. ANZEIGE
# Zeige plots an (falls)


# Um den Code zu terminieren müssen alle Plots geschlossen werden oder Umgekehrt
print('\033[94m' + " Um den Code zu terminieren müssen alle Plots geschlossen werden oder Umgekehrt" + '\033[94m')

# Plots anzeigen (ERST ZUM SCHLUSS, SONST STOCKT CODE!)
plt.show()


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region REFERENZEN
# Quellen (im Code angegeben als "Ref [n]")


# [1]
# "How do I multiply each element in a list by a number?", Stackoverflow, 2016
# https://stackoverflow.com/questions/35166633/how-do-i-multiply-each-element-in-a-list-by-a-number/35166717
# Accessed 03.11.2020

# [2]
# "Truncate to three decimals in Python", Stackoverflow, 2011
# https://stackoverflow.com/questions/8595973/truncate-to-three-decimals-in-python
# Accessed 03.11.2020

# [3]
# "How to change fonts in matplotlib (python)?", Stackoverflow, 2016
# https://stackoverflow.com/questions/21321670/how-to-change-fonts-in-matplotlib-python
# Accessed 10.11.2020

# [4]
# "Add an extra information in a python plot?", Stackoverflow, 2013
# https://stackoverflow.com/questions/16066695/add-an-extra-information-in-a-python-plot
# Accessed 17.11.2020

# [5]
# "Generate a heatmap in MatPlotLib using a scatter data set", Stackoverflow, 2017
# https://stackoverflow.com/questions/2369492/generate-a-heatmap-in-matplotlib-using-a-scatter-data-set
# Accessed 17.11.2020

# [6]
# "Matplotlib: imshow with second y axis", Stackoverflow, 2018
# https://stackoverflow.com/questions/48255824/matplotlib-imshow-with-second-y-axis
# Accessed 17.11.2020

# [7]
# "python matplotlib heatmap colorbar from transparent", Stackoverflow, 2018
# https://stackoverflow.com/questions/51601272/python-matplotlib-heatmap-colorbar-from-transparent
# Accessed 17.11.2020

# [8]
# "Named colors in matplotlib", Stackoverflow, 2016
# https://stackoverflow.com/questions/22408237/named-colors-in-matplotlib
# Accessed 17.11.2020


# endregion
