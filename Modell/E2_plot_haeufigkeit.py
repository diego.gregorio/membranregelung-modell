# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>
#
#
# Author: Diego Gregorio (2020)
#
# MEMBRANREGELUNG MODELL
# MSc Projekt 2020 Umweltingenieurwissenschaften ETH Zürich
#
# E2_plot_haeufigkeit - Hilfsfunktion zur Darstellung von (kumulativer) Häufigkeitsverteilung


# Nötige Packages laden
import matplotlib.pyplot as plt
import numpy


def plot_haeufigkeit(dataset, str_x, figsize=[6.4, 4.8], bins='auto'):
    # plot_haeufigkeit() plottet die Häufigkeitsverteilung und Kumulative Häufigkeitsverteilung
    #
    # Inputs:
    #   dataset     [Pandas Serie] Datensatz welches analysiert werden soll
    #   str_x       [String] Name des Datensatzes und Einheit (x-Label)
    #   figsize     [Array] Plot Breite und Höhe in Zoll (Fakultativ, sonst Default)
    #   bin_num     [Integer oder Array] Anzahl Bins oder deren Grenzwerte (Fakultativ, sonst automatisch)
    #
    # Outputs:
    #   fig         [matplotlib] Plot
    #   ax1         [matplotlib] Linke Y-Axis (Zeitanteil)
    #   ax2         [matplotlib] Rechte Y-Axis (Kumulativer Zeitanteil)

    # Plot erstellen
    fig, ax1 = plt.subplots(figsize=figsize)

    # Linke y-Axis (Ref [1], [2] & [3]) - Relative Häufigkeitsverteilung
    color = 'tab:blue'
    ax1.set_xlabel(str_x)
    ax1.set_ylabel('Zeitanteil [-]', color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    counts, bins = numpy.histogram(dataset, bins=bins)
    ax1.hist(bins[:-1], bins, weights=counts/numpy.sum(counts), color=color)

    # Rechte y-Axis (Ref [4] & [5]) - Kumulative relative Häufigkeitsverteilung
    color = 'tab:red'
    ax2 = ax1.twinx()
    ax2.set_ylabel('Kumulativer Zeitanteil [-]', color=color)
    ax2.tick_params(axis='y', labelcolor=color)
    ax2.set_ylim(bottom=0)
    if dataset.size != 0:
        ax2.plot(dataset.sort_values(), numpy.arange(0, 1, 1/dataset.size), color=color)
    else:
        ax2.plot(0, numpy.arange(0, 1, 1), color=color)

    # Abschliessen
    fig.tight_layout()

    return fig, ax1, ax2


# Quellen (im Code angegeben als "Ref [n]")

# [1]
# "Calculating optimal number of bins in a histogram", Stackexchange, 2010
# https://stats.stackexchange.com/questions/798/calculating-optimal-number-of-bins-in-a-histogram
# Accessed 03.11.2020

# [2]
# "matplotlib.pyplot.hist", matplotlib, 2020
# https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.hist.html
# Accessed 03.11.2020

# [3]
# "Setting a relative frequency in a matplotlib histogram", Stackoverflow, 2012
# https://stackoverflow.com/questions/9767241/setting-a-relative-frequency-in-a-matplotlib-histogram
# Accessed 04.11.2020

# [4]
# "Plots with different scales", Matplotlib, 2018
# https://matplotlib.org/gallery/api/two_scales.html
# Accessed 03.11.2020

# [5]
# "cumulative distribution plots python", Stackoverflow, 2013
# https://stackoverflow.com/questions/15408371/cumulative-distribution-plots-python
# Accessed 03.11.2020
