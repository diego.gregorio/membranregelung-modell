# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>
#
#
# Author: Diego Gregorio (2020)
#
# MEMBRANREGELUNG MODELL
# MSc Projekt 2020 Umweltingenieurwissenschaften ETH Zürich
#
# A2_energie - Hilfsfunktion zur Berechnung des Energieverbrauchs von Gebläse- und Pumpen


def energie(punkt, strassen, Q_LeapHi, Q_LeapLo, p_LeapHi, p_LeapLo, eta_LeapHi, eta_LeapLo, dauer):
    # energie() berechnet den Energie-Aufwand von Gebläse und Pumpen gemäss Pumpenleistungsformel
    #
    # Inputs:
    #   punkt       [-] Betriebspunkt (von 0 bis 2*strassen)
    #   strassen    [-] Anzahl Strassen
    #   Q_LeapHi    [m3/s] Durchfluss bei LEAP-Hi
    #   Q_LeapLo    [m3/s] Durchfluss bei LEAP-Lo
    #   p_LeapHi    [m] Druckhöhe bei LEAP-Hi
    #   p_LeapLo    [m] Druckhöhe bei LEAP-Lo
    #   eta_LeapHi  [-] Wirkungsgrad bei LEAP-Hi
    #   eta_LeapLo  [-] Wirkungsgrad bei LEAP-Lo
    #   dauer       [s] Wie Lange in Betrieb
    #
    # Outputs:
    #   energie_out [kWh] Energieverbrauch

    # Totale Anzahl Betriebspunkte OHNE Nullpunkt
    tot_punkt = strassen * 2

    # Betriebspunkt in unterer Hälfte
    if punkt <= strassen:
        n_LeapLo = punkt  # Betriebspunkt entspricht genau der Anzahl von Strassen in Leap-Lo, auch 0 berücksichtigt
        n_LeapHi = 0
    # Obere Hälfte
    else:
        n_LeapHi = punkt - strassen  # Leap-Hi wird progressiv dazugeschaltet auf Kosten von Leap-Lo
        n_LeapLo = tot_punkt - punkt

    # Pumpenleistung [W]
    g = 9.81  # [m/s2]
    phi = 1000  # [kg/m3]
    P_LeapHi = phi * g * (Q_LeapHi * n_LeapHi) * p_LeapHi / eta_LeapHi
    P_LeapLo = phi * g * (Q_LeapLo * n_LeapLo) * p_LeapLo / eta_LeapLo

    # Totales Power [kW]
    P_tot = (P_LeapHi + P_LeapLo) / 1000

    # Dauer in [h]
    dauer = dauer / 60 / 60

    # Energie-Verbrauch über die Betriebsdauer [kWh]
    energie_out = P_tot * dauer

    return energie_out
