# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>
#
#
# Author: Diego Gregorio (2020)
#
# MEMBRANREGELUNG MODELL
# MSc Projekt 2020 Umweltingenieurwissenschaften ETH Zürich
#
# F1_analyse_varianten - Vergleich der simulierten Varianten


# ----------------------------------------------------------------------------------------------------------------------


# region INDEX
# Überblick der Programmstruktur


# 1. PACKAGES
#    Nötige Funktionen und Packages laden
# 2. EINGABE
#    TODO: User Inputs
# 3. EINLESEN
#    Lese benötigte Datensätze ein
# 4. VERGLEICH
#    Plot-Vergleich von ausgewählten Charakteristiken
# 5. ANZEIGE
#    Zeige plots an (falls)
# REFERENZEN


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 1. PACKAGES
# Nötige Funktionen und Packages laden


# Funktion plot_haeufigkeit
from E2_plot_haeufigkeit import plot_haeufigkeit


# matplotlib.pyplot (um Plots zu erstellen)
try:
    import matplotlib.pyplot as plt
except ImportError as e:
    print('\033[93m' + "matplotlib nicht installiert! 'pip install matplotlib' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# numpy (um Berechnungen mit arrays durchzuführen)
try:
    import numpy
except ImportError as e:
    print('\033[93m' + "numpy nicht installiert! 'pip install numpy' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# os (um Order zum speichern zu erstellen)
try:
    import os
except ImportError as e:
    print('\033[93m' + "os nicht installiert! 'pip install os' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# pandas (um mit Serien/Dataframes zu arbeiten)
try:
    import pandas as pd
except ImportError as e:
    print('\033[93m' + "pandas nicht installiert! 'pip install pandas' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.

# pickle (um gespeicherte Daten zu importieren)
try:
    import pickle
except ImportError as e:
    print('\033[93m' + "pickle nicht installiert! 'pip install pickle' im Terminal nötig!" + '\033[0m')
    pass  # module doesn't exist, deal with it.


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 2. EINGABE
# TODO: User Inputs


# Path zum Ordner mit den zu vergleichende Varianten und Path zum speichern der Resultate
varianten_path = 'VV_varianten/XX_Resultate zum Vergleichen/Pufferhoehe Erhoehung/'
save_path = "XX_outputs/F1_varianten/Pufferhohe Erhoehung"

# Varianten zu Vergleichen (Name des jeweiligen Ordner), deren Anzahl Strassen und Zwischenbelüftungszeit
varianten_name = ["Referenz", "+0.5m", "+1.0m", "+2.0m"]
strassen = [4, 4, 4, 4]
mixing_t = [2*60, 2*60, 2*60, 2*60]  # [s]

# Simulationszeitschritt in [s]
timestep_s = 5 * 60  # (5min)

# Initialisierungszeit in [s] (wird von allen abgeschnitten)
init = 1 * 30 * 24 * 60 * 60  # 1 Monat

# Gewollte Plot Dimensionen [Zoll] und Schriftart (Ref [3])
size_fig_bar = [6, len(varianten_name)/1.5+1]  # Breite, Höhe der Barplots
size_fig_hae = [6, 4]  # Breite, Höhe der Häufigkeitsverteilung-Plots
plt.rcParams["font.family"] = "arial"
plt.rcParams["font.size"] = 11

# Ordner erstellen um zu speichern
if not os.path.exists(save_path):
    os.makedirs(save_path)


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 3. EINLESEN
# Lese benötigte Datensätze ein


# Listen von Listen zum speichern initialisieren (siehe Ref [1] & [2])
data_Qe = [[] for ll in range(len(varianten_name))]
data_bp = [[] for ll in range(len(varianten_name))]
data_L_betrieb = [[] for ll in range(len(varianten_name))]
data_L_mixing = [[] for ll in range(len(varianten_name))]
data_leap_hi = [[] for ll in range(len(varianten_name))]
data_leap_lo = [[] for ll in range(len(varianten_name))]
data_h = [[] for ll in range(len(varianten_name))]

# Initialisierung in Datensatz-Zeitschritte
init = int(init / timestep_s)

# Daten sammeln in Listen von Listen (Ref [1] & [2])
jj = -1
for ii in varianten_name:
    jj += 1
    # Einlesen
    out_Qe = pd.Series(pickle.load(open(varianten_path + ii + "/out_Qe.p", "rb"))).iloc[init:]
    out_bp = pd.Series(pickle.load(open(varianten_path + ii + "/out_bp.p", "rb"))).iloc[init:]
    out_L_betrieb = pd.Series(pickle.load(open(varianten_path + ii + "/out_L_betrieb.p", "rb"))).iloc[init:]
    out_L_mixing = pd.Series(pickle.load(open(varianten_path + ii + "/out_L_mixing.p", "rb"))).iloc[init:]
    out_leap_lo = pd.Series(pickle.load(open(varianten_path + ii + "/out_leap_lo.p", "rb"))).iloc[init:]
    out_leap_hi = pd.Series(pickle.load(open(varianten_path + ii + "/out_leap_hi.p", "rb"))).iloc[init:]
    out_h = pd.Series(pickle.load(open(varianten_path + ii + "/out_h.p", "rb"))).iloc[init:]
    # Speichern
    data_Qe[jj].append(out_Qe)
    data_bp[jj].append(out_bp)
    data_L_betrieb[jj].append(out_L_betrieb)
    data_L_mixing[jj].append(out_L_mixing)
    data_leap_hi[jj].append(out_leap_hi)
    data_leap_lo[jj].append(out_leap_lo)
    data_h[jj].append(out_h)


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 4. VERGLEICH
# Plot-Vergleich von ausgewählten Charakteristiken


# Notentlastung-Verteilung
fig, ax1 = plt.subplots(figsize=size_fig_hae)
ax2 = ax1.twinx()
jj = 0
for ii in varianten_name:
    counts, bins = numpy.histogram(data_Qe[jj], bins='auto')
    ax1.hist(bins[:-1], bins, weights=counts / numpy.sum(counts), alpha=1/len(varianten_name))
    ax2.plot(numpy.transpose(numpy.sort(numpy.array(data_Qe[jj]))), numpy.arange(0, 1, 1 / numpy.array(data_Qe[jj]).size))
    jj += 1
ax1.set_ylabel('Zeitanteil [-]')
ax2.set_ylabel('Kumulativer Zeitanteil [-]')
ax2.set_ylim(bottom=0)
ax1.set_xlabel('Notentlastungen [$\mathregular{m^3}$/s]')
ax2.legend(varianten_name, loc='center right')
fig.tight_layout()
fig.savefig(save_path + "/Qe.svg")


# Betriebspunkte-Verteilung
fig, ax1 = plt.subplots(figsize=size_fig_hae)
ax2 = ax1.twinx()
jj = 0
for ii in varianten_name:
    counts, bins = numpy.histogram(data_bp[jj], bins=numpy.insert(numpy.unique(data_bp[jj]), 0, numpy.amin(numpy.array(data_bp[jj])) - 1) + 0.5)  # Bin Edges setzen (minimaler Wert -1 einsetzen, damit unterster Edge, dann alle Werte +0.5, damit Bins zentriert)
    ax1.hist(bins[:-1], bins, weights=counts / numpy.sum(counts), alpha=1/len(varianten_name))
    ax2.plot(numpy.transpose(numpy.sort(numpy.array(data_bp[jj]))), numpy.arange(0, 1, 1 / numpy.array(data_bp[jj]).size))
    jj += 1
ax1.set_ylabel('Zeitanteil [-]')
ax2.set_ylabel('Kumulativer Zeitanteil [-]')
ax2.set_ylim(bottom=0)
ax1.set_xlabel('Betriebspunkt')
ax2.legend(varianten_name, loc='center right')
fig.tight_layout()
fig.savefig(save_path + "/bp.svg")


# Strassenbetrieb (im Gegensatz zu E1_analyse_outputs.py wird hier mit Listen statt pd.Serien gearbeitet)
fig, ax1 = plt.subplots(figsize=size_fig_bar)
jj = 0
for ii in varianten_name:

    # Betrieb pro Strasse
    leap_lo_tot = numpy.sum(numpy.array(data_leap_lo[jj]) / strassen[jj])  # Totale Zeitschritte in LEAP-Lo pro Strasse
    leap_hi_tot = numpy.sum(numpy.array(data_leap_hi[jj]) / strassen[jj])  # Totale Zeitschritte in LEAP-Hi pro Strasse
    leap_aus_tot = numpy.array(data_leap_lo[jj]).size - leap_lo_tot - leap_hi_tot  # Auszeit pro Strasse (ergibt sich durch Zeitdifferenz)
    leap_tot = leap_lo_tot + leap_hi_tot + leap_aus_tot

    # Zwischenbelüftungszeitschritte (Teil der Auszeit) pro Strasse (Ref [3])
    leap_mixing = (numpy.array(data_L_mixing[jj]) > 0).sum() * mixing_t[jj] / timestep_s / strassen[jj]

    # Horizonatler stackbarplot (Ref [4], [5] & [6])
    ax1.barh(ii, leap_lo_tot / leap_tot, color='papayawhip', hatch='/')
    ax1.barh(ii, leap_hi_tot / leap_tot, color='pink', hatch='x', left=leap_lo_tot/leap_tot)
    ax1.barh(ii, leap_aus_tot / leap_tot, color='lightblue', left=(leap_lo_tot + leap_hi_tot)/leap_tot)
    ax1.barh(ii, leap_mixing / leap_tot, color='xkcd:blue/grey', left=(leap_lo_tot + leap_hi_tot)/leap_tot)

    # Zahlen auf Balken (Ref [7])
    ax1.text(leap_lo_tot / leap_tot / 2, ii, str('%.5f' % (leap_lo_tot / leap_tot)), horizontalalignment='center', verticalalignment='center')
    ax1.text((leap_lo_tot + leap_hi_tot / 2) / leap_tot, ii, str('%.5f' % (leap_hi_tot / leap_tot)), horizontalalignment='center', verticalalignment='center')
    ax1.text((leap_lo_tot + leap_hi_tot + leap_mixing + leap_aus_tot / 2) / leap_tot, ii, str('%.5f' % ((leap_aus_tot - leap_mixing) / leap_tot)), horizontalalignment='center', verticalalignment='center')
    ax1.text((leap_lo_tot + leap_hi_tot + leap_mixing)/leap_tot, jj + 0.39, "Mixing=" + str('%.5f' % (leap_mixing / leap_tot)), horizontalalignment='left', verticalalignment='bottom', color='xkcd:faded blue')

    jj += 1

plt.rcParams['hatch.linewidth'] = 0.3  # (Ref [8])
ax1.invert_yaxis()
ax1.set_xlabel('Zeitanteil pro Strasse [-]')
ax1.set_xlim(right=1)
ax1.legend(["LEAP Lo", "LEAP Hi", "AUS"], loc='lower center', bbox_to_anchor=(0.5, 1.01), ncol=3, borderaxespad=0, frameon=False)  # (Ref [9])
fig.tight_layout()
ax1.spines["right"].set_visible(False)  # (Ref [11])
ax1.spines["left"].set_visible(False)
ax1.spines["top"].set_visible(False)
fig.savefig(save_path + "/strassen.svg")


# Luftmengen
fig, ax1 = plt.subplots(figsize=size_fig_bar)
jj = 0
for ii in varianten_name:

    # Simulationszeit in [a]
    timestep_tot = numpy.array(data_L_betrieb[jj]).size
    years = timestep_tot * timestep_s /60/60/24/365

    # Totale Luftmenge [1/a]]
    L_betrieb_tot = numpy.sum(data_L_betrieb[jj]) / years
    L_mixing_tot = numpy.sum(data_L_mixing[jj]) / years

    # Horizonatler stackbarplot (Ref [4], [5] & [6])
    ax1.barh(ii, L_betrieb_tot, color='k')
    ax1.barh(ii, L_mixing_tot, color='xkcd:faded blue', left=L_betrieb_tot)

    # Zahlen auf Balken (Ref [7] & [10])
    ax1.text(L_betrieb_tot / 2, ii, str('{:,.0f}'.format(L_betrieb_tot).replace(',', '´')), horizontalalignment='center', verticalalignment='center', color='whitesmoke')
    ax1.text(L_betrieb_tot, jj + 0.33, "Mixing=" + str('{:,.0f}'.format(int(L_mixing_tot)).replace(',', '´')), horizontalalignment='right', verticalalignment='bottom', color='xkcd:faded blue')

    # Referenzlinie (Erster Eintrag gilt als Referenz)
    if jj == 0:
        L_referenz = L_betrieb_tot + L_mixing_tot
        ax1.vlines(L_referenz, -0.5, len(varianten_name) - 0.5, colors='r')
    else:
        L_abweichung = L_betrieb_tot + L_mixing_tot - L_referenz
        ax1.arrow(L_referenz, jj, L_abweichung, 0, length_includes_head=True, head_length=abs(L_abweichung)/10, head_width=0.05, color='r')
        if L_abweichung > 0:  # Bedingung hat nur Einfluss auf 'horizontalalignment='
            ax1.text(L_referenz, jj-0.05, str('%+.2f' %(L_abweichung/L_referenz*100) + '%'), horizontalalignment='left', verticalalignment='bottom', color='r')
        else:
            ax1.text(L_referenz, jj-0.05, str('%+.2f' %(L_abweichung/L_referenz*100) + '%'), horizontalalignment='right', verticalalignment='bottom', color='r')

    jj += 1

ax1.invert_yaxis()
ax1.set_xlabel('Luftmenge [$\mathregular{Nm^3}$/a]')
fig.tight_layout()
ax1.spines["right"].set_visible(False)  # (Ref [11])
ax1.spines["left"].set_visible(False)
ax1.spines["top"].set_visible(False)
fig.savefig(save_path + "/L.svg")


# Wasserspiegel
fig, ax1 = plt.subplots(figsize=size_fig_hae)
ax2 = ax1.twinx()
jj = 0
for ii in varianten_name:
    counts, bins = numpy.histogram(data_h[jj], bins='auto')
    ax1.hist(bins[:-1], bins, weights=counts / numpy.sum(counts), alpha=1/len(varianten_name))
    ax2.plot(numpy.transpose(numpy.sort(numpy.array(data_h[jj]))), numpy.arange(0, 1, 1 / numpy.array(data_h[jj]).size))
    jj += 1
ax1.set_ylabel('Zeitanteil [-]')
ax2.set_ylabel('Kumulativer Zeitanteil [-]')
ax2.set_ylim(bottom=0)
ax1.set_xlabel('Wasserspiegel [m]')
ax2.legend(varianten_name, loc='center right')
fig.tight_layout()
fig.savefig(save_path + "/h.svg")


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region 5. ANZEIGE
# Zeige plots an (falls)


# Um den Code zu terminieren müssen alle Plots geschlossen werden oder Umgekehrt
print('\033[94m' + " Um den Code zu terminieren müssen alle Plots geschlossen werden oder Umgekehrt" + '\033[94m')

# Plots anzeigen (ERST ZUM SCHLUSS, SONST STOCKT CODE!)
plt.show()


# endregion


# ----------------------------------------------------------------------------------------------------------------------


# region REFERENZEN
# Quellen (im Code angegeben als "Ref [n]")


# [1]
# "Creating an empty Pandas DataFrame, then filling it?", Stackoverflow, 2019
# https://stackoverflow.com/questions/13784192/creating-an-empty-pandas-dataframe-then-filling-it
# Accessed 10.11.2020

# [2]
# "How to create and initialize a list of lists in python?", thispointer.com, 2020
# https://thispointer.com/how-to-create-and-initialize-a-list-of-lists-in-python/
# Accessed 10.11.2020

# [3]
# "Count all values in a matrix greater than a value", Stackoverflow, 2012
# https://stackoverflow.com/questions/12995937/count-all-values-in-a-matrix-greater-than-a-value
# Accessed 11.11.2020

# [4]
# "How do you divide each element in a list by an int?", Stackoverflow, 2015
# https://stackoverflow.com/questions/8244915/how-do-you-divide-each-element-in-a-list-by-an-int
# Accessed 11.11.2020

# [5]
# "Named colors in matplotlib", Stackoverflow, 2014
# https://stackoverflow.com/questions/22408237/named-colors-in-matplotlib
# Accessed 11.11.2020

# [6]
# "Stacked barplot", matplotlib.org, 2012
# https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/bar_stacked.html
# Accessed 11.11.2020

# [7]
# "How to display the value of the bar on each bar with pyplot.barh()?", Stackoverflow, 2015
# https://stackoverflow.com/questions/30228069/how-to-display-the-value-of-the-bar-on-each-bar-with-pyplot-barh
# Accessed 13.11.2020

# [8]
# "How to change the linewidth of hatch in matplotlib?", Stackoverflow, 2015
# https://stackoverflow.com/questions/29549530/how-to-change-the-linewidth-of-hatch-in-matplotlib
# Accessed 11.11.2020

# [9]
# "How To Put Legend outside of Axes Properly in Matplotlib?", jdhao's blog, 2018
# https://jdhao.github.io/2018/01/23/matplotlib-legend-outside-of-axes/
# Accessed 20.11.2020

# [10]
# "How to print number with commas as thousands separators?", Stackoverflow, 2019
# https://stackoverflow.com/questions/1823058/how-to-print-number-with-commas-as-thousands-separators
# Accessed 17.11.2020

# [11]
# "How to remove the frame from a Matplotlib figure in Python", Kite, 2020
# https://www.kite.com/python/answers/how-to-remove-the-frame-from-a-matplotlib-figure-in-python
# Accessed 18.11.2020


# endregion
