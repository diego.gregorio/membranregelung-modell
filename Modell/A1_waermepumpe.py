# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>
#
#
# Author: Diego Gregorio (2020)
#
# MEMBRANREGELUNG MODELL
# MSc Projekt 2020 Umweltingenieurwissenschaften ETH Zürich
#
# A1_waermepumpe - Hilfsfunktion zur Simulation der nachgeschalteten Wärmepumpe


def waermepumpe(T, Tmin, COP, kompressor, Qin, Tin, V, c_wasser, rho_wasser, timestep):
    # warmepumpe() berechnet den Wärmegewinn durch die Wärmepumpe im Speicher und die resultierende Temperatur-Änderung
    #
    # Inputs:
    #   T           [°C] Wassertemperatur im Speicher bei Zeit t
    #   Tmin        [°C] Minimale Wassertemperatur (draunter stellt Wärmepumpe den Betrieb ein)
    #   COP         [-] Wärmepumpe Effizienz
    #   kompressor  [kW] Verbrauch des Kompressors
    #   Qin         [m3/s] Zufluss zum Speicher
    #   Tin         [°C] Zufluss Temperatur
    #   V           [m3] Speichervolumen
    #   c_wasser    [kJ/kg/K] Wärmekapazität von Wasser
    #   rho_wasser  [kg/m3] Wasserdichte
    #   timestep    [s] Simulationsschritte
    #
    # Outputs:
    #   waermegewinn [kW] Wärmenutzung durch Wärmepumpe
    #   T            [°C] Temperatur nach Wärmenutzung

    # Volumen-Abhängige Wärmekapazität
    c_wasser = c_wasser * rho_wasser  # [kJ/m3/K]

    # Temperaturen in [K]
    T = T + 273
    Tin = Tin + 273

    # Wärmegewinn berechnen
    if T > Tmin:  # Nur wenn minimale Temperatur im Speicher
        waermegewinn = COP * kompressor - kompressor  # [kW = kJ/s]
    else:
        waermegewinn = 0

    # Neue Temperatur im Speicher gemäss Zufluss und Wärmeabfuhr berechnen
    if (Qin*timestep) > V:
        T = Tin  # Komplett neues Wasser
    else:
        # Bilanz in [kJ] (dT/dt*V*c = Tin*Vin*c - T*Vab*c - waermegewinn)
        T = (Qin * timestep * Tin * c_wasser - Qin * timestep * T * c_wasser - waermegewinn * timestep) / (V * c_wasser) + T

    # Ausgabe (T in [°C])
    T = T - 273
    return waermegewinn, T